import requests, json, pprint

headers = {'content-type': 'application/x-www-form-urlencoded', 'charset':'utf-8'}
data = {'login': 'admin', 'password': 'admin', 'db': 'galago.ng'}
base_url = 'http://odoo.ng'

req = requests.get('{}/api/auth/token'.format(base_url), data=data, headers=headers)
content = json.loads(req.content.decode('utf-8'))
headers['refresh-token'] = content.get('access_token')
headers['access-token'] = content.get('access_token')
print(headers, content)
# req = requests.delete('{}/api/auth/token'.format(base_url), data=data, headers=headers)

# partner record

# req = requests.get('{}/api/res.partner/'.format(base_url), headers=headers,
#                  data=json.dumps({'limit': 1}))
# ***Pass optional parameter like this***
# {
#   'limit': 10, 'filters': "[('supplier','=',True),('parent_id','=', False)]",
#   'order': 'name asc', 'offset': 10
# }

# print(req.content)
# print(req, headers)